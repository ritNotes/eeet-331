N=1024;
T=2;
t=linspace(0,T,N);
ya=zeros(size(t));
ind=find(t>1&t<2);
ya(ind)=A*abs(sin(2*pi*t(ind)));
figure(111);
plot(t,ya);
axis([0 T -1 A+1]); grid on;
