N=1024;
repeatTime=3;
%Each cycle takes 3t to complete
T=repeatTime*3;
t=linspace(0,T,N);
A=2;
yc=zeros(size(t));
for i=0:3:(T)
    ind=find(t>(i)&t<(i+3));
    yc(ind)=-A*triangularPulse(i,(i+1),(i+3),t(ind))+A;
end
figure(131);
plot(t,yc);
axis([0 T -1 A+1]); grid on;
