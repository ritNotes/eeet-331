N=1024;
t=linspace(0,1,N);
ya=zeros(size(t));
A=3;
ind=find(t>0&t<0.5);
ya(ind)=A*abs(sin(2*pi*t(ind)));
figure(110);
plot(t,ya);
axis([0 3 -1 A+1]); grid on;
