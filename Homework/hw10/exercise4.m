N=1024;
repeatTime=1;
%Each cycle takes 9t to complete
T=repeatTime*9;
t=linspace(0,T,N);
A=5;
yc=zeros(size(t));
for i=0:9:(T)
    ind=find(t>(i+1)&t<(i+2)|t>(i+7)&t<(i+8));
    yc(ind)=A;
end
figure(132);
plot(t,yc);
axis([0 T -1 A+1]); grid on;
