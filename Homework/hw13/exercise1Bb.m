num1=[0.9980 1];den1=[1 -2*0.9980 1];
y1=dimpulse(num1,den1);
figure(1);plot(y1);axis([0, 500, -35, 35]);title('Figure 2: Sine Wave');
exportgraphics(gcf,'exercise1Bb.eps','ContentType','vector');
