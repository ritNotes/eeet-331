init();
M=15;
T=2*pi;
t=0:0.1:20;
[m,cm]=create_cm_series(M,T);
y=cm2yt(t,T,m,cm,M);
make_plot(t,real(y),"Exercise 10 Graph","t","y")